# NGX Build Info

Gather useful information about the build and CVS then find/replace in your built app.

## Install

```sh
npm i @rippell/ngx-build-info -D
```

## Basic Usage

Add variables to your codebase that can be found/replaced after the build has completed.

Easiest way is adding to your `index.html`:

```html
<script>
window.app = {
  hash: '${gitHash}',
  built: '${buildTime}'
}
</script>
```

Otherwise, variables can be added to other files and scanned there -- see below.

---

Add a `postbuild` to your `package.json` scripts:

```json
  ...
  "scripts": {
    ...
    "postbuild": "ngx-build-info"
  }
  ...
```

By default, this will attempt a find/replace for variables in your `index.html`.
Other options are:
* `main` - all `main-*.js` files
* `all` - all files in the `./dist/*/` folder
* RegExp - accepts anything else as a regular expression for finding the files

For example if you know it only needs ran against the common package:

```json
    "postbuild": "ngx-build-info common-.*\.js"
```

---

For a full list of variables, see the keys of the [values object](./ngx-build-info.js#L6).
